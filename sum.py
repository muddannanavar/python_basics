#!/usr/bin/env python

import sys

num1 = int(sys.argv[1])
num2 = int(sys.argv[2])
sum = num1 + num2

print(f"sum of number 1 : {num1} and number 2 : {num2} = {sum}")